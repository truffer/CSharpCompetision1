/*
//MoPhy
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Intrinsics.X86;

namespace matrix_multiplacation
{
public static class Mophy_Programme
{
    /*
    static float[,] G = new float[4, 3]
    {
            {1f, 0f, 0f},
            {0.5f, 0.5f, 0.5f},
            {0.5f, -0.5f, 0.5f},
            {0f, 0f, 1f}
    };

    static float[,] GT = new float[3, 4]
    {
            {1, 0.5f, 0.5f, 0},
            { 0, 0.5f, -0.5f, 0 },
            { 0, 0.5f, 0.5f, 1 }
    };

    static float[,] BT = new float[4, 4]
    {
            {1, 0, -1, 0},
            { 0, 1, 1, 0 },
            { 0, -1, 1, 0 },
            { 0, 1, 0, -1 }
    };
    static float[,] B = new float[4, 4]
    {
            {1, 0, 0, 0},
            { 0, 1, -1, 1 },
            { -1, 1, 1, 0 },
            { 0, 0, 0, -1 }
    };
    static float[,] AT = new float[2, 4]
    {
            {1, 1, 1, 0},
            {0, 1, -1, -1}
    };
    static float[,] A = new float[4, 2]
    {
            {1, 0},
            { 1, 1 },
            { 1, -1 },
            { 0, 1 }
    };

    static float[,] doGgGT(float[,] g)
    {
        float[,] tmp = new float[4, 3];
        uint K = 3;
        uint M = 4;
        uint N = 3;
        for (uint m = 0; m < M; m++)
        {
            for (uint n = 0; n < N; n++)
            {
                float val = 0.0f;
                for (uint k = 0; k < K; k++)
                {
                    val += G[m, k] * g[k, n];
                }
                tmp[m, n] = val;
            }
        }
        float[,] ret = new float[4, 4];
        K = 3; M = 4; N = 4;
        for (uint m = 0; m < M; m++)
        {
            for (uint n = 0; n < N; n++)
            {
                float val = 0.0f;
                for (uint k = 0; k < K; k++)
                {
                    val += tmp[m, k] * GT[k, n];
                }
                ret[m, n] = val;
            }
        }
        return ret;
    }

    static float[][] doBTdB(float[][] d)
    {
        float[][] tmp = new float[4][];
        uint K = 4;
        uint M = 4;
        uint N = 4;
        for (uint m = 0; m < M; m++)
        {
            for (uint n = 0; n < N; n++)
            {
                float val = 0.0f;
                for (uint k = 0; k < K; k++)
                {
                    val += BT[m, k] * d[k][n];
                }
                tmp[m][n] = val;
            }
        }
        float[][] ret = new float[4][];
        for (uint m = 0; m < M; m++)
        {
            for (uint n = 0; n < N; n++)
            {
                float val = 0.0f;
                for (uint k = 0; k < K; k++)
                {
                    val += tmp[m][k] * B[k, n];
                }
                ret[m][n] = val;
            }
        }
        return ret;
    }

    static float[][] doElementMul(float[,] g, float[][] d)
    {
        float[][] ret = new float[4][];
        uint M = 4;
        uint N = 4;
        for (uint m = 0; m < M; m++)
        {
            for (uint n = 0; n < N; n++)
            {
                ret[m][n] = g[m, n] * d[m][n];
            }
        }
        return ret;
    }

    static float[][] doATaA(float[][] a)
    {
        float[][] tmp = new float[2][];
        uint K = 4;
        uint M = 2;
        uint N = 4;
        for (uint m = 0; m < M; m++)
        {
            for (uint n = 0; n < N; n++)
            {
                float val = 0.0f;
                for (uint k = 0; k < K; k++)
                {
                    val += AT[m, k] * a[k][n];
                }
                tmp[m][n] = val;
            }
        }
        float[][] ret = new float[2][];
        K = 4; M = 2; N = 2;
        for (uint m = 0; m < M; m++)
        {
            for (uint n = 0; n < N; n++)
            {
                float val = 0.0f;
                for (uint k = 0; k < K; k++)
                {
                    val += tmp[m][k] * A[k, n];
                }
                ret[m][n] = val;
            }
        }
        return ret;
    }
    
    static void Main(string[] args)
    {
        List<float[][]> image = new List<float[][]>(2048);
        float[,] filter = new float[3, 3];
        List<float[][]> rets = new List<float[][]>(2048);

        for (int i = 0; i < image.ToArray().Length; i++)
        {
            for (int m = 0; m < 4; m++)
            {
                for (int n = 0; n < 4; n++)
                {
                    image[i][m][n] = (m + 1) * 1.0f;
                }

            }
        }
        for (int m = 0; m < 3; m++)
        {
            for (int n = 0; n < 3; n++)
            {
                filter[m, n] = 1.0f;
            }
        }
        DateTime beforDT = System.DateTime.Now;
        for (int j = 0; j < image.ToArray().Length; j++)
        {
            rets[j] = doATaA(doElementMul(doGgGT(filter), doBTdB(image[j])));
        }
        DateTime afterDT = System.DateTime.Now;
        TimeSpan ts = afterDT.Subtract(beforDT);
        Console.Write("-----");
        Console.Write(ts.TotalMilliseconds);
        Console.Write("\n");
    }
}
}
*/
