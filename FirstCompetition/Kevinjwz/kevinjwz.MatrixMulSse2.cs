//MatrixMulSse2.cs

using System;
using System.Runtime.CompilerServices;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;
using size_t = nuint;
using m128d = System.Runtime.Intrinsics.Vector128<double>;

unsafe struct MatrixMulSse2 : IMatrixMulSimdSpecific
{
	public size_t BlockRows => 228;
	public size_t BlockCols => 128;
	public size_t KernelRows => 4;
	public size_t KernelCols => 4;

	public void PackBlock(double* dst, double* src, size_t src_rows, size_t src_cols, size_t src_stride)
	{
		PackBlockBy4Cols(dst, src, src_rows, src_cols, src_stride);
	}

	public void PackVPanel(double* dst, double* src, size_t src_rows, size_t src_cols, size_t src_stride)
	{
		PackVPanelBy4Rows(dst, src, src_rows, src_cols, src_stride);
	}

	public void MulKernelComplete(bool add, double* C, double* A, double* B, size_t A_cols, size_t C_stride)
	{
		MulKernel4x4(add, C, A, B, A_cols, C_stride);
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public void MulKernelIncomplete(bool add, double* C, double* A, double* B, size_t C_rows, size_t C_cols, size_t A_cols, size_t C_stride)
	{
		if (C_cols > 2)
			MulKernelMx4(add, C, A, B, C_rows, C_cols, A_cols, C_stride);
		else
			MulKernelMx2(add, C, A, B, C_rows, C_cols, A_cols, C_stride);
	}


	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	static void PackVPanelBy4Rows(double* dst, double* src, size_t src_rows, size_t src_cols, size_t src_stride)
	{
		size_t i = 0;
		for (; i + 4 <= src_rows; i += 4)
		{
			double* src0 = src + (i + 0) * src_stride;
			double* src1 = src + (i + 1) * src_stride;
			double* src2 = src + (i + 2) * src_stride;
			double* src3 = src + (i + 3) * src_stride;

			size_t j = 0;
			for (; j + 2 <= src_cols; j += 2)
			{
				m128d v0 = Sse2.LoadVector128(src0 + j);
				m128d v1 = Sse2.LoadVector128(src1 + j);
				m128d v2 = Sse2.LoadVector128(src2 + j);
				m128d v3 = Sse2.LoadVector128(src3 + j);

				Transpose2x2(ref v0, ref v1);
				Transpose2x2(ref v2, ref v3);

				Sse2.StoreAligned(dst + 2 * 0, v0);
				Sse2.StoreAligned(dst + 2 * 1, v2);
				Sse2.StoreAligned(dst + 2 * 2, v1);
				Sse2.StoreAligned(dst + 2 * 3, v3);
				dst += 4 * 2;
			}

			if (j < src_cols)
			{
				*(dst + 0) = *(src0 + j);
				*(dst + 1) = *(src1 + j);
				*(dst + 2) = *(src2 + j);
				*(dst + 3) = *(src3 + j);
				dst += 4;
			}
		}
		if (i < src_rows)
		{
			size_t src_rows_rem = src_rows - i;
			double* src0 = src + (i + 0) * src_stride;
			double* src1 = src + (i + 1) * src_stride;
			double* src2 = src + (i + 2) * src_stride;
			for (size_t j = 0; j < src_cols; ++j)
			{
				*(dst + 0) = *(src0 + j); if (src_rows_rem == 1) goto L_next_col;
				*(dst + 1) = *(src1 + j); if (src_rows_rem == 2) goto L_next_col;
				*(dst + 2) = *(src2 + j);
			L_next_col:
				dst += src_rows_rem;
			}
		}
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	static void Transpose2x2(ref m128d m0, ref m128d m1)
	{
		m128d a0a1 = m0;
		m128d b0b1 = m1;
		m128d a0b0 = Sse2.UnpackLow(a0a1, b0b1);
		m128d a1b1 = Sse2.UnpackHigh(a0a1, b0b1);
		m0 = a0b0;
		m1 = a1b1;
	}

	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	static void PackBlockBy4Cols(double* dst, double* src, size_t src_rows, size_t src_cols, size_t src_stride)
	{
		size_t packed_rows = (src_cols + 3) / 4;
		double* dst_last_row = dst + (packed_rows - 1) * src_rows * 4;
		//size_t last_row_step = src_cols % 4 <= 2 ? 2 : 4;
		// ��Ϊ��
		size_t last_row_step = (size_t)((src_cols % 4 <= 2) ? 2 : 4);

		for (size_t i = 0; i < src_rows; ++i)
		{
			double* src_row = src + i * src_stride;
			double* dst_col = dst + i * 4;

			size_t j = 0;
			for (; j + 4 <= src_cols; j += 4)
			{
				m128d v0 = Sse2.LoadVector128(src_row + j + 0);
				m128d v1 = Sse2.LoadVector128(src_row + j + 2);
				Sse2.StoreAligned(dst_col + 0, v0);
				Sse2.StoreAligned(dst_col + 2, v1);
				dst_col += src_rows * 4;
			}

			if (j < src_cols)
			{
				for (size_t j2 = 0; j2 < last_row_step; ++j2, ++j)
					dst_last_row[j2] = j < src_cols ? src_row[j] : 0;

				dst_last_row += last_row_step;
			}
		}
	}

	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	static void MulKernel4x4(bool add, double* C, double* A, double* B, size_t A_cols, size_t C_stride)
	{
		m128d sum00, sum10, sum20, sum30;
		m128d sum01, sum11, sum21, sum31;
		sum00 = sum10 = sum20 = sum30 = m128d.Zero;
		sum01 = sum11 = sum21 = sum31 = m128d.Zero;

		double* a0 = A, b0 = B;
		size_t A_cols_2 = A_cols & ~(size_t)1;
		size_t k = 0;
		for (; k < A_cols_2; k += 2)
		{
			const size_t prefetch_offset = 64 * 4;
			Sse.Prefetch0((char*)a0 + prefetch_offset);
			Sse.Prefetch0((char*)b0 + prefetch_offset);

			m128d va, vb0, vb1;
			vb0 = Sse2.LoadAlignedVector128(b0);
			vb1 = Sse2.LoadAlignedVector128(b0 + 2);

			va = Broadcast(a0);
			sum00 = Sse2.Add(sum00, Sse2.Multiply(vb0, va));
			sum01 = Sse2.Add(sum01, Sse2.Multiply(vb1, va));

			va = Broadcast(a0 + 1);
			sum10 = Sse2.Add(sum10, Sse2.Multiply(vb0, va));
			sum11 = Sse2.Add(sum11, Sse2.Multiply(vb1, va));

			va = Broadcast(a0 + 2);
			sum20 = Sse2.Add(sum20, Sse2.Multiply(vb0, va));
			sum21 = Sse2.Add(sum21, Sse2.Multiply(vb1, va));

			va = Broadcast(a0 + 3);
			sum30 = Sse2.Add(sum30, Sse2.Multiply(vb0, va));
			sum31 = Sse2.Add(sum31, Sse2.Multiply(vb1, va));

			vb0 = Sse2.LoadAlignedVector128(b0 + 4);
			vb1 = Sse2.LoadAlignedVector128(b0 + 6);

			va = Broadcast(a0 + 4);
			sum00 = Sse2.Add(sum00, Sse2.Multiply(vb0, va));
			sum01 = Sse2.Add(sum01, Sse2.Multiply(vb1, va));

			va = Broadcast(a0 + 5);
			sum10 = Sse2.Add(sum10, Sse2.Multiply(vb0, va));
			sum11 = Sse2.Add(sum11, Sse2.Multiply(vb1, va));

			va = Broadcast(a0 + 6);
			sum20 = Sse2.Add(sum20, Sse2.Multiply(vb0, va));
			sum21 = Sse2.Add(sum21, Sse2.Multiply(vb1, va));

			va = Broadcast(a0 + 7);
			sum30 = Sse2.Add(sum30, Sse2.Multiply(vb0, va));
			sum31 = Sse2.Add(sum31, Sse2.Multiply(vb1, va));

			a0 += 8;
			b0 += 8;
		}
		if (k < A_cols)
		{
			m128d va, vb0, vb1;
			vb0 = Sse2.LoadAlignedVector128(b0);
			vb1 = Sse2.LoadAlignedVector128(b0 + 2);

			va = Broadcast(a0);
			sum00 = Sse2.Add(sum00, Sse2.Multiply(vb0, va));
			sum01 = Sse2.Add(sum01, Sse2.Multiply(vb1, va));

			va = Broadcast(a0 + 1);
			sum10 = Sse2.Add(sum10, Sse2.Multiply(vb0, va));
			sum11 = Sse2.Add(sum11, Sse2.Multiply(vb1, va));

			va = Broadcast(a0 + 2);
			sum20 = Sse2.Add(sum20, Sse2.Multiply(vb0, va));
			sum21 = Sse2.Add(sum21, Sse2.Multiply(vb1, va));

			va = Broadcast(a0 + 3);
			sum30 = Sse2.Add(sum30, Sse2.Multiply(vb0, va));
			sum31 = Sse2.Add(sum31, Sse2.Multiply(vb1, va));
		}


		double* c0, c1, c2, c3;
		c0 = C + C_stride * 0;
		c1 = C + C_stride * 1;
		c2 = C + C_stride * 2;
		c3 = C + C_stride * 3;

		if (add)
		{
			sum00 = Sse2.Add(sum00, Sse2.LoadVector128(c0)); sum01 = Sse2.Add(sum01, Sse2.LoadVector128(c0 + 2));
			sum10 = Sse2.Add(sum10, Sse2.LoadVector128(c1)); sum11 = Sse2.Add(sum11, Sse2.LoadVector128(c1 + 2));
			sum20 = Sse2.Add(sum20, Sse2.LoadVector128(c2)); sum21 = Sse2.Add(sum21, Sse2.LoadVector128(c2 + 2));
			sum30 = Sse2.Add(sum30, Sse2.LoadVector128(c3)); sum31 = Sse2.Add(sum31, Sse2.LoadVector128(c3 + 2));
		}
		Sse2.Store(c0, sum00); Sse2.Store(c0 + 2, sum01);
		Sse2.Store(c1, sum10); Sse2.Store(c1 + 2, sum11);
		Sse2.Store(c2, sum20); Sse2.Store(c2 + 2, sum21);
		Sse2.Store(c3, sum30); Sse2.Store(c3 + 2, sum31);
	}

	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	static void MulKernelMx4(bool add, double* C, double* A, double* B, size_t C_rows, size_t C_cols, size_t A_cols, size_t C_stride)
	{
		m128d sum00, sum10, sum20, sum30;
		m128d sum01, sum11, sum21, sum31;
		sum00 = sum10 = sum20 = sum30 = m128d.Zero;
		sum01 = sum11 = sum21 = sum31 = m128d.Zero;

		size_t k = 0;
		{
			goto loop_check;
		loop_update:
			k += 1;
		loop_check:
			if (k >= A_cols) goto loop_end;

			double* a0 = A, b0 = B;
			A += C_rows;
			B += 4;
			m128d va, vb0, vb1;
			vb0 = Sse2.LoadAlignedVector128(b0);
			vb1 = Sse2.LoadAlignedVector128(b0 + 2);

			va = Broadcast(a0);
			sum00 = Sse2.Add(sum00, Sse2.Multiply(vb0, va));
			sum01 = Sse2.Add(sum01, Sse2.Multiply(vb1, va));
			if (C_rows == 1) goto loop_update;

			va = Broadcast(a0 + 1);
			sum10 = Sse2.Add(sum10, Sse2.Multiply(vb0, va));
			sum11 = Sse2.Add(sum11, Sse2.Multiply(vb1, va));
			if (C_rows == 2) goto loop_update;

			va = Broadcast(a0 + 2);
			sum20 = Sse2.Add(sum20, Sse2.Multiply(vb0, va));
			sum21 = Sse2.Add(sum21, Sse2.Multiply(vb1, va));
			if (C_rows == 3) goto loop_update;

			va = Broadcast(a0 + 3);
			sum30 = Sse2.Add(sum30, Sse2.Multiply(vb0, va));
			sum31 = Sse2.Add(sum31, Sse2.Multiply(vb1, va));
			goto loop_update;
		loop_end:;
		}

		double* c0, c1, c2, c3;
		c0 = C + C_stride * 0;
		c1 = C + C_stride * 1;
		c2 = C + C_stride * 2;
		c3 = C + C_stride * 3;

		if (C_cols == 4)
		{
			while (add)
			{
				sum00 = Sse2.Add(sum00, Sse2.LoadVector128(c0)); sum01 = Sse2.Add(sum01, Sse2.LoadVector128(c0 + 2)); if (C_rows == 1) break;
				sum10 = Sse2.Add(sum10, Sse2.LoadVector128(c1)); sum11 = Sse2.Add(sum11, Sse2.LoadVector128(c1 + 2)); if (C_rows == 2) break;
				sum20 = Sse2.Add(sum20, Sse2.LoadVector128(c2)); sum21 = Sse2.Add(sum21, Sse2.LoadVector128(c2 + 2)); if (C_rows == 3) break;
				sum30 = Sse2.Add(sum30, Sse2.LoadVector128(c3)); sum31 = Sse2.Add(sum31, Sse2.LoadVector128(c3 + 2)); break;
			}

			while (true)
			{
				Sse2.Store(c0, sum00); Sse2.Store(c0 + 2, sum01); if (C_rows == 1) break;
				Sse2.Store(c1, sum10); Sse2.Store(c1 + 2, sum11); if (C_rows == 2) break;
				Sse2.Store(c2, sum20); Sse2.Store(c2 + 2, sum21); if (C_rows == 3) break;
				Sse2.Store(c3, sum30); Sse2.Store(c3 + 2, sum31); break;
			}
		}
		else
		{
			while (add)
			{
				sum00 = Sse2.Add(sum00, Sse2.LoadVector128(c0)); sum01 = Sse2.Add(sum01, Sse2.LoadScalarVector128(c0 + 2)); if (C_rows == 1) break;
				sum10 = Sse2.Add(sum10, Sse2.LoadVector128(c1)); sum11 = Sse2.Add(sum11, Sse2.LoadScalarVector128(c1 + 2)); if (C_rows == 2) break;
				sum20 = Sse2.Add(sum20, Sse2.LoadVector128(c2)); sum21 = Sse2.Add(sum21, Sse2.LoadScalarVector128(c2 + 2)); if (C_rows == 3) break;
				sum30 = Sse2.Add(sum30, Sse2.LoadVector128(c3)); sum31 = Sse2.Add(sum31, Sse2.LoadScalarVector128(c3 + 2)); break;
			}

			while (true)
			{
				Sse2.Store(c0, sum00); Sse2.StoreScalar(c0 + 2, sum01); if (C_rows == 1) break;
				Sse2.Store(c1, sum10); Sse2.StoreScalar(c1 + 2, sum11); if (C_rows == 2) break;
				Sse2.Store(c2, sum20); Sse2.StoreScalar(c2 + 2, sum21); if (C_rows == 3) break;
				Sse2.Store(c3, sum30); Sse2.StoreScalar(c3 + 2, sum31); break;
			}
		}
	}

	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	static void MulKernelMx2(bool add, double* C, double* A, double* B, size_t C_rows, size_t C_cols, size_t A_cols, size_t C_stride)
	{
		m128d sum00, sum10, sum20, sum30;
		sum00 = sum10 = sum20 = sum30 = m128d.Zero;

		size_t k = 0;
		{
			goto loop_check;
		loop_update:
			k += 1;
		loop_check:
			if (k >= A_cols) goto loop_end;

			double* a0 = A, b0 = B;
			A += C_rows;
			B += 2;
			m128d va, vb0;
			vb0 = Sse2.LoadAlignedVector128(b0);

			va = Broadcast(a0);
			sum00 = Sse2.Add(sum00, Sse2.Multiply(vb0, va));
			if (C_rows == 1) goto loop_update;

			va = Broadcast(a0 + 1);
			sum10 = Sse2.Add(sum10, Sse2.Multiply(vb0, va));
			if (C_rows == 2) goto loop_update;

			va = Broadcast(a0 + 2);
			sum20 = Sse2.Add(sum20, Sse2.Multiply(vb0, va));
			if (C_rows == 3) goto loop_update;

			va = Broadcast(a0 + 3);
			sum30 = Sse2.Add(sum30, Sse2.Multiply(vb0, va));
			goto loop_update;
		loop_end:;
		}

		double* c0, c1, c2, c3;
		c0 = C + C_stride * 0;
		c1 = C + C_stride * 1;
		c2 = C + C_stride * 2;
		c3 = C + C_stride * 3;

		if (C_cols == 2)
		{
			while (add)
			{
				sum00 = Sse2.Add(sum00, Sse2.LoadVector128(c0)); if (C_rows == 1) break;
				sum10 = Sse2.Add(sum10, Sse2.LoadVector128(c1)); if (C_rows == 2) break;
				sum20 = Sse2.Add(sum20, Sse2.LoadVector128(c2)); if (C_rows == 3) break;
				sum30 = Sse2.Add(sum30, Sse2.LoadVector128(c3)); break;
			}

			while (true)
			{
				Sse2.Store(c0, sum00); if (C_rows == 1) break;
				Sse2.Store(c1, sum10); if (C_rows == 2) break;
				Sse2.Store(c2, sum20); if (C_rows == 3) break;
				Sse2.Store(c3, sum30); break;
			}
		}
		else
		{
			while (add)
			{
				sum00 = Sse2.Add(sum00, Sse2.LoadScalarVector128(c0)); if (C_rows == 1) break;
				sum10 = Sse2.Add(sum10, Sse2.LoadScalarVector128(c1)); if (C_rows == 2) break;
				sum20 = Sse2.Add(sum20, Sse2.LoadScalarVector128(c2)); if (C_rows == 3) break;
				sum30 = Sse2.Add(sum30, Sse2.LoadScalarVector128(c3)); break;
			}

			while (true)
			{
				Sse2.StoreScalar(c0, sum00); if (C_rows == 1) break;
				Sse2.StoreScalar(c1, sum10); if (C_rows == 2) break;
				Sse2.StoreScalar(c2, sum20); if (C_rows == 3) break;
				Sse2.StoreScalar(c3, sum30); break;
			}
		}
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	static m128d Broadcast(double* a)
	{
		if (Sse3.IsSupported)
			return Sse3.LoadAndDuplicateToVector128(a);
		m128d t = Sse2.LoadScalarVector128(a);

		return Sse.Shuffle(t.AsSingle(), t.AsSingle(), 0b01_00_01_00).AsDouble();
	}

	public void Dispose() { }
}
