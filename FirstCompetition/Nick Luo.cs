//Nick Luo
//On Visual Studio 2019
//Platform target: x64
//Framework: .Net Core 3.1
// cs
using System;
using System.Numerics;
using System.Threading.Tasks;

//namespace MatrixConsoleApp
//{
public class Nickluo_Programme
{
    public class Matrix
    {
        public Matrix(int m, int n)
        {
            Rows = m;
            Cols = n;
            Data = new double[m * n];
        }
        public Matrix(double[] source, int m, int n)
        {
            if (m * n != source.Length)
                throw new InvalidOperationException("Array length not matched!");
            Rows = m;
            Cols = n;
            Data = source;
        }
        public Matrix(double[,] source)
        {
            Rows = source.GetLength(0);
            Cols = source.GetLength(1);
            Data = new double[Rows * Cols];
            Buffer.BlockCopy(source, 0, Data, 0, (int)(Rows * Cols * sizeof(double)));
        }
        public double[] Data { get; }
        public ref double this[int i, int j] => ref Data[i * Cols + j];
        public int Rows { get; }
        public int Cols { get; }
        private static readonly Random RandomObj = new Random();
        public static Matrix GenerateRandom(int m, int n)
        {
            var matrix = new Matrix(m, n);
            for (var i = 0; i < n * m; ++i)
                matrix.Data[i] = RandomObj.NextDouble();
            return matrix;
        }
    };

    public static Matrix MatrixMultipleFast(Matrix ma, Matrix mb)
    {
        if (ma.Cols != mb.Rows)
            return null;
        var result = new Matrix(ma.Rows, mb.Cols);
        var count = Vector<double>.Count;
        Parallel.For(0, mb.Cols, j =>
        {
            Span<double> mbCol = stackalloc double[mb.Rows];
            var maSpan = ma.Data.AsSpan();
            var mbSpan = mb.Data.AsSpan();
            for (var row = 0; row < mb.Rows; ++row)
                mbCol[row] = mbSpan[row * mb.Cols + j];
            for (var i = 0; i < ma.Rows; ++i)
            {
                var n = 0;
                var sum = 0.0;
                for (; n <= ma.Cols - count; n += count)
                {
                    var mbSegV = new Vector<double>(mbCol.Slice(n));
                    var maSegV = new Vector<double>(maSpan.Slice(i * ma.Cols + n));
                    sum += Vector.Dot(maSegV, mbSegV);
                }
                for (; n < ma.Cols; ++n)
                    sum += maSpan[i * ma.Cols + n] * mbSpan[n * mb.Cols + j];
                result[i, j] = sum;
            }
        });
        return result;
    }
    /*
    public static void Main(string[] args)
    {
        var m = 2000;
        var n = 2000;
        var k = 2000;
        if (args.Length >= 3)
        {
            m = int.Parse(args[0]);
            n = int.Parse(args[1]);
            k = int.Parse(args[2]);
            if (m < 2000 || m > 2049 || n < 2000 || n > 2049 || k < 2000 || k > 2049)
            {
                Console.WriteLine("Dimension value should be between 2000 and 2049 for this demo!");
                return;
            }
        }
        else
        {
            Console.WriteLine("Performance of Matrix A x Matrix B. ");
            Console.WriteLine("Parameters:");
            Console.WriteLine("    m : rows of Matrix A (2000-2049)");
            Console.WriteLine("    n : columns of Matrix A and rows of Matrix B (2000-2049)");
            Console.WriteLine("    k : columns of Matrix B (2000-2049)");
            Console.WriteLine("Usage:");
            Console.WriteLine("    " + AppDomain.CurrentDomain.FriendlyName + " m n k");
            Console.WriteLine("Now run with default Matrix(2000,2000) x Matrix(2000,2000)");
            Console.WriteLine("Note: data in matrix is randomly generated for demonstration.");
            Console.WriteLine();
        }

        var ma = Matrix.GenerateRandom(m, n);
        var mb = Matrix.GenerateRandom(n, k);

        var ts0 = DateTime.Now.Ticks / 10000;
        var result0 = MatrixMultipleFast(ma, mb);
        var ts1 = DateTime.Now.Ticks / 10000 - ts0;
        if (result0 == null)
        {
            Console.WriteLine("Invalid Inputs!");
            return;
        }
        Console.WriteLine("MatrixMultipleFast() cost: " + ts1 + "ms");
    }
    */
    /// <summary>
    /// Nick Luo����˷�
    /// Truffer����Nick Luo�����޸�
    /// </summary>
    /// <param name="m">����A Row Number</param>
    /// <param name="n">����A Column Number</param>
    /// <param name="k">����B Column Number</param>
    /// <param name="matrix_r_out">����C Row Number</param>
    /// <param name="matrix_c_out">����C Column Number</param>
    /// <param name="matrix_a">����A����</param>
    /// <param name="matrix_b">����B����</param>
    /// <param name="probe_row">���row</param>
    /// <param name="probe_col">���column</param>
    /// <param name="t1024">���C[probe_row,probe_row]����</param>
    /// <returns>���ؼ���ʱ�䣨���룩</returns>
    public static double Execute(int m, int n, int k, out int matrix_r_out, out int matrix_c_out, double[,] matrix_a, double[,] matrix_b, int probe_row, int probe_col, out double t1024)
    {
        var ma = new Matrix(matrix_a);
        var mb = new Matrix(matrix_b);

        DateTime beforDT = System.DateTime.Now;
        var result0 = MatrixMultipleFast(ma, mb);

        matrix_r_out = result0.Rows;
        matrix_c_out = result0.Cols;
        t1024 = result0[probe_row, probe_col];

        DateTime afterDT = System.DateTime.Now;
        TimeSpan ts = afterDT.Subtract(beforDT);
        if (result0 == null)
        {
            throw new Exception("Invalid Inputs!");
        }
        return (ts.TotalMilliseconds);
    }
}
//}
