//涂山苏苏
//使用的命名空间:Legal.Truffer
using System;
using System.Numerics;
using System.Threading.Tasks;

using Legal.Truffer;

//函数被写在类里面。

public class Tushansusu_Programme
{
    public static Matrix matrix_mul(Matrix X, Matrix Y)
    {
        int R = X.Rows;
        int C = Y.Columns;
        int n = X.Columns;

        double[,] temp = Y.ToArray();
        double[,] Z = new double[R, C];

        int i, j, k;

        for (i = 0; i < R; i++)
        {
            double[] tempx = X.row(i);
            for (k = 0; k < n; k++)
            {
                if (tempx[k] != 0)
                {
                    for (j = 0; j < C; j++)
                    {
                        Z[i, j] += tempx[k] * temp[k, j];
                    }
                }
            }
        }
        return new Matrix(Z);
    }
}
