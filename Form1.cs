﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Legal.Truffer;

namespace Truffer201030
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Random rnd = new Random();
        WebBrowser browser;
        int matrix_size_m = 2017;
        int matrix_size_n = 2027;
        int matrix_size_k = 2029;
        double[,] matrix_a;
        double[,] matrix_b;
        int loop_count = 1;
        int probe_row = 1023;
        int probe_col = 1303;

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Text = "第一届C#数值计算编程比赛__评判程序__北京联高软件开发有限公司（2020）";
            splitContainer1.Dock = DockStyle.Fill;
            splitContainer1.SplitterDistance = 185;

            button1.Cursor = Cursors.Hand;
            button2.Cursor = Cursors.Hand;
            button3.Cursor = Cursors.Hand;
            button4.Cursor = Cursors.Hand;
            button5.Cursor = Cursors.Hand;
            button6.Cursor = Cursors.Hand;
            button7.Cursor = Cursors.Hand;
            button8.Cursor = Cursors.Hand;
            button9.Cursor = Cursors.Hand;

            browser = new WebBrowser();
            browser.Parent = splitContainer1.Panel2;
            browser.Dock = DockStyle.Fill;

            // 初始化矩阵A B；赋值随机双精度实数；
            matrix_a = new double[matrix_size_m, matrix_size_n];
            for (int i = 0; i < matrix_size_m; i++)
            {
                for (int j = 0; j < matrix_size_n; j++)
                {
                    matrix_a[i, j] = rnd.NextDouble();
                }
            }
            matrix_b = new double[matrix_size_n, matrix_size_k];
            for (int i = 0; i < matrix_size_n; i++)
            {
                for (int j = 0; j < matrix_size_k; j++)
                {
                    matrix_b[i, j] = rnd.NextDouble();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("选手：<b>ink</b><br>");
                sb.AppendLine("评判：题目要求“双精度实数矩阵”,也就是数据类型必须是<font color=blue>double</font>类型的，而选手提交的是<font color=blue>float</font>类型的，不符合要求。<br>");
                sb.AppendLine("结论：遗憾出局！<br>");
                sb.AppendLine("<img src='http://5b0988e595225.cdn.sohucs.com/images/20180427/3479097489544246bf0f96f86f19d8aa.gif'><br>");
                browser.DocumentText = sb.ToString();
            }
            catch (Exception ex)
            {
                browser.DocumentText = ex.Message;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("选手：<b>LEI Hongfaan</b><br>");
                sb.AppendLine("矩阵：a[" + matrix_a.GetLength(0) + "," + matrix_a.GetLength(1) + "]<br>");
                sb.AppendLine("矩阵：b[" + matrix_b.GetLength(0) + "," + matrix_b.GetLength(1) + "]<br>");
                double time_sum = 0.0;
                for (int i = 0; i < loop_count; i++)
                {
                    DateTime beforDT = System.DateTime.Now;
                    double[,] matrix_c = LEIHongfaan_Programme.Multiply(matrix_a, matrix_b);
                    DateTime afterDT = System.DateTime.Now;
                    TimeSpan ts = afterDT.Subtract(beforDT);
                    if (i == 0)
                    {
                        sb.AppendLine("矩阵：c[" + matrix_c.GetLength(0) + "," + matrix_c.GetLength(1) + "]<br>");
                    }
                    sb.AppendLine("测点：c[" + probe_row + "," + probe_col + "]=" + matrix_c[probe_row, probe_col] + "<br>");
                    sb.AppendLine("算时[" + i + "]：" + ts.TotalMilliseconds + " Milliseconds<br>");
                    time_sum += ts.TotalMilliseconds;
                }
                sb.AppendLine("平均：" + (time_sum / loop_count) + " Milliseconds<br>");
                sb.AppendLine("<img src='http://5b0988e595225.cdn.sohucs.com/images/20181030/12c011ef02dc4219a62f9b418f775592.gif'><br>");
                browser.DocumentText = sb.ToString();
            }
            catch (Exception ex)
            {
                browser.DocumentText = ex.Message;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("选手：<b>Nickluo</b><br>");
                sb.AppendLine("矩阵：a[" + matrix_a.GetLength(0) + "," + matrix_a.GetLength(1) + "]<br>");
                sb.AppendLine("矩阵：b[" + matrix_b.GetLength(0) + "," + matrix_b.GetLength(1) + "]<br>");
                double time_sum = 0.0;
                for (int i = 0; i < loop_count; i++)
                {
                    double ts = Nickluo_Programme.Execute(matrix_size_m, matrix_size_n, matrix_size_k, out int matrix_size_r_out, out int matrix_size_c_out, matrix_a, matrix_b, probe_row, probe_col, out double t1024);
                    if (i == 0)
                    {
                        sb.AppendLine("矩阵：c[" + matrix_size_r_out + "," + matrix_size_c_out + "]<br>");
                    }
                    sb.AppendLine("测点：c[" + probe_row + "," + probe_col + "]=" + t1024 + "<br>");
                    sb.AppendLine("算时[" + i + "]: " + ts + " Milliseconds<br>");
                    time_sum += ts;
                }
                sb.AppendLine("平均：" + (time_sum / loop_count) + " Milliseconds<br>");
                sb.AppendLine("<img src='http://5b0988e595225.cdn.sohucs.com/images/20181030/12c011ef02dc4219a62f9b418f775592.gif'><br>");
                browser.DocumentText = sb.ToString();
            }
            catch (Exception ex)
            {
                browser.DocumentText = ex.Message;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("选手：<b>时光荏苒</b><br>");
                sb.AppendLine("矩阵：a[" + matrix_a.GetLength(0) + "," + matrix_a.GetLength(1) + "]<br>");
                sb.AppendLine("矩阵：b[" + matrix_b.GetLength(0) + "," + matrix_b.GetLength(1) + "]<br>");
                double time_sum = 0.0;
                for (int i = 0; i < loop_count; i++)
                {
                    DateTime beforDT = System.DateTime.Now;
                    double[,] matrix_c = Shiguangrenrang_Programme.MatrixMult(matrix_a, matrix_b);
                    DateTime afterDT = System.DateTime.Now;
                    TimeSpan ts = afterDT.Subtract(beforDT);
                    if (i == 0)
                    {
                        sb.AppendLine("矩阵：c[" + matrix_c.GetLength(0) + "," + matrix_c.GetLength(1) + "]<br>");
                    }
                    sb.AppendLine("测点：c[" + probe_row + "," + probe_col + "]=" + matrix_c[probe_row, probe_col] + "<br>");
                    sb.AppendLine("算时[" + i + "]: " + ts.TotalMilliseconds + " Milliseconds<br>");
                    time_sum += ts.TotalMilliseconds;
                }
                sb.AppendLine("平均：" + (time_sum / loop_count) + " Milliseconds<br>");
                sb.AppendLine("<img src='http://5b0988e595225.cdn.sohucs.com/images/20181030/12c011ef02dc4219a62f9b418f775592.gif'><br>");
                browser.DocumentText = sb.ToString();
            }
            catch (Exception ex)
            {
                browser.DocumentText = ex.Message;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("选手：<b>nieyufan</b><br>");
                sb.AppendLine("结果：代码无法使用。遗憾出局！<br>");
                sb.AppendLine("<img src='http://5b0988e595225.cdn.sohucs.com/images/20180427/3479097489544246bf0f96f86f19d8aa.gif'><br>");
                browser.DocumentText = sb.ToString();
            }
            catch (Exception ex)
            {
                browser.DocumentText = ex.Message;
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                Matrix matrix_t_a = new Matrix(matrix_a);
                Matrix matrix_t_b = new Matrix(matrix_b);
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("选手：<b>涂山苏苏</b><br>");
                sb.AppendLine("矩阵：a[" + matrix_a.GetLength(0) + "," + matrix_a.GetLength(1) + "]<br>");
                sb.AppendLine("矩阵：b[" + matrix_b.GetLength(0) + "," + matrix_b.GetLength(1) + "]<br>");
                double time_sum = 0.0;
                for (int i = 0; i < loop_count; i++)
                {
                    DateTime beforDT = System.DateTime.Now;
                    Matrix matrix_t_c = Tushansusu_Programme.matrix_mul(matrix_t_a, matrix_t_b);
                    DateTime afterDT = System.DateTime.Now;
                    TimeSpan ts = afterDT.Subtract(beforDT);
                    if (i == 0)
                    {
                        sb.AppendLine("矩阵：c[" + matrix_t_c.Rows + "," + matrix_t_c.Columns + "]<br>");
                    }
                    sb.AppendLine("测点：c[" + probe_row + "," + probe_col + "]=" + matrix_t_c[probe_row, probe_col] + "<br>");
                    sb.AppendLine("算时[" + i + "]: " + ts.TotalMilliseconds + " Milliseconds<br>");
                    time_sum += ts.TotalMilliseconds;
                }
                sb.AppendLine("平均：" + (time_sum / loop_count) + " Milliseconds<br>");
                sb.AppendLine("<img src='http://5b0988e595225.cdn.sohucs.com/images/20181030/12c011ef02dc4219a62f9b418f775592.gif'><br>");
                browser.DocumentText = sb.ToString();
            }
            catch (Exception ex)
            {
                browser.DocumentText = ex.Message;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("选手：<b>Mophy</b><br>");
                sb.AppendLine("评判：题目要求“双精度实数矩阵”,也就是数据类型必须是<font color=blue>double</font>类型的，而选手提交的是<font color=blue>float</font>类型的，不符合要求。<br>");
                sb.AppendLine("结论：遗憾出局！<br>");
                sb.AppendLine("<img src='http://5b0988e595225.cdn.sohucs.com/images/20180427/3479097489544246bf0f96f86f19d8aa.gif'><br>");
                browser.DocumentText = sb.ToString();
            }
            catch (Exception ex)
            {
                browser.DocumentText = ex.Message;
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("选手：<b>John</b><br>");
                sb.AppendLine("矩阵：a[" + matrix_a.GetLength(0) + "," + matrix_a.GetLength(1) + "]<br>");
                sb.AppendLine("矩阵：b[" + matrix_b.GetLength(0) + "," + matrix_b.GetLength(1) + "]<br>");
                double time_sum = 0.0;
                for (int i = 0; i < loop_count; i++)
                {
                    double ts = JohnSpace.John_Programme.Execute(matrix_size_m, matrix_size_n, matrix_size_n, matrix_size_k, matrix_a, matrix_b, out int matrix_size_r_out, out int matrix_size_c_out, probe_row, probe_col, out double t1024);
                    if (i == 0)
                    {
                        sb.AppendLine("矩阵：c[" + matrix_size_r_out + "," + matrix_size_c_out + "]<br>");
                    }
                    sb.AppendLine("测点：c[" + probe_row + "," + probe_col + "]=" + t1024 + "<br>");
                    sb.AppendLine("算时[" + i + "]: " + ts + " Milliseconds<br>");
                    time_sum += ts;
                }
                sb.AppendLine("平均：" + (time_sum / loop_count) + " Milliseconds<br>");
                sb.AppendLine("<img src='http://5b0988e595225.cdn.sohucs.com/images/20181030/12c011ef02dc4219a62f9b418f775592.gif'><br>");
                browser.DocumentText = sb.ToString();
            }
            catch (Exception ex)
            {
                browser.DocumentText = ex.Message;
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            try
            {
                var mul = MatrixMultipliers.Create();
                var matrix_c = new double[matrix_size_m, matrix_size_k]; // the result array

                StringBuilder sb = new StringBuilder();
                sb.AppendLine("选手：<b>Kevin jwz</b><br>");
                sb.AppendLine("矩阵：a[" + matrix_size_m + "," + matrix_size_n + "]<br>");
                sb.AppendLine("矩阵：b[" + matrix_size_n + "," + matrix_size_k + "]<br>");
                double time_sum = 0.0;
                for (int i = 0; i < loop_count; i++)
                {
                    DateTime beforDT = System.DateTime.Now;
                    mul.Multiply(matrix_c, matrix_a, matrix_b);
                    DateTime afterDT = System.DateTime.Now;
                    TimeSpan ts = afterDT.Subtract(beforDT);
                    if (i == 0)
                    {
                        sb.AppendLine("矩阵：c[" + matrix_c.GetLength(0) + "," + matrix_c.GetLength(1) + "]<br>");
                    }
                    sb.AppendLine("测点：c[" + probe_row + "," + probe_col + "]=" + matrix_c[probe_row, probe_col] + "<br>");
                    sb.AppendLine("算时[" + i + "]: " + ts.TotalMilliseconds + " Milliseconds<br>");
                    time_sum += ts.TotalMilliseconds;
                }
                sb.AppendLine("平均：" + (time_sum / loop_count) + " Milliseconds<br>");
                //sb.AppendLine("备注：未按参赛者要求设置“Windows如何启用2MB内存页”。<br>");
                sb.AppendLine("<img src='http://5b0988e595225.cdn.sohucs.com/images/20181030/12c011ef02dc4219a62f9b418f775592.gif'><br>");
                browser.DocumentText = sb.ToString();

                mul.Dispose();
            }
            catch (Exception ex)
            {
                browser.DocumentText = ex.Message;
            }
        }
    }
}
